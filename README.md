### Heat Watcher in Ruby

In short, the HEAT stack output is horrendous to read and see components that are failing / waiting.

This knockup sorts it into readable output.

Run using:
```bash
./heat-watch.rb --stack <your-stack-name-here>
```

It desaults to using colours, so you will need the colorize gem:

```bash
gem install colorize
```

Alternitavely just switch off colours:

```bash
./heat-watch.rb --stack <your-stack-name-here> --no-colour
```

And to run in a watch loop of just the waiting stack elements:

```bash
watch --color ./heat-watch.rb --stack <your-stack-name-here> --no-show-all
```

All flags can be seen here:

```bash
./heat-watch.rb --help
```
