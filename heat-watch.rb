#!/usr/bin/env ruby

require 'logger'
require 'pp'
require 'optparse'
require 'colorize'

log = Logger.new(STDOUT)

options = {
  :colour   => true,
  :show_all => true,
}
OptionParser.new do |opts|
  opts.banner = "Usage: heat-watch.rb [options] --stack [stack-name]"

  opts.on("-v", "--verbose", "Run with verbose logging") do |v|
    options[:verbose] = true
  end
  opts.on("-s", "--stack STACKNAME", "Stack name") do |v|
    options[:stack] = v
  end
  opts.on("-c", "--[no-]colour", "Show output in ansii colour" ) do |v|
    options[:colour] = v
  end
  opts.on("-a", "--[no-]show-all", "Shows all the elements of the stack - Default is: true") do |v|
    options[:show_all] = v
  end
  opts.on("-h", "--help", "Show this output") do |v|
    puts opts
    exit(0)
  end
end.parse!

unless options[:stack].is_a? String
  log.error("No stack defined with -s or --stack")
  exit(1)
end
unless options[:verbose]
  log.level = Logger::WARN
else
  log.level = Logger::DEBUG
end

log.debug("#{options}")

@stack_elements = {}

cmd = "openstack stack event list #{options[:stack]} --nested-depth=30 | awk '{print $3\" \"$4}' | tr -d '[]:'"
stack_log = `#{cmd}`

stack_log.each_line do | line |
  log.debug("#{line}")
  format_line = line.split(" ")
  log.debug("Space 0: #{format_line[0]} space 1: #{format_line[1]}")
  @stack_elements["#{format_line[0]}"] = "#{format_line[1]}"
end

log.debug("#{@stack_elements}")
#pp @stack_elements

# Print it out
log.debug("Use colours: #{options[:colour]}")
@stack_elements.each do |key, val|
  if val =~ /FAILED/
    if options[:colour] == true
      log.debug("Printing failed lines - with colour as #{options[:colour]}")
      puts "#{key}:" + " #{val}".colorize(:red)
    else
      log.debug("Printing failed lines - without colour")
      puts "#{key}:" + " #{val}"
    end
  elsif val !~ /CREATE_COMPLETE/ and val !~ /SIGNAL_COMPLETE/ and val !~ /DELETE_COMPLETE/
    if options[:colour] == true
      log.debug("Printing non completed lines - with colour as #{options[:colour]}")
      puts "#{key}:" + " #{val}".colorize(:blue)
    else
      log.debug("Printing non complete lines - without colour")
      puts "#{key}:" + " #{val}"
    end
  elsif options[:show_all] == true
    log.debug("Log show all => #{options[:show_all]}")
    if options[:colour] == true
      log.debug("Colour is true - printing completed lines")
      puts "#{key}:" + " #{val}".colorize(:green)
    else
      log.debug("Colour is false - just printing vars - this is #{options[:colour]}")
      puts "#{key}:" + " #{val}"
    end
  end
end
